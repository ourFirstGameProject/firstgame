﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheckForObjects : MonoBehaviour {

	
	// Update is called once per frame
	void Update () {
        RaycastHit hit;

        if(Physics.Raycast(transform.position, -Vector3.up, out hit, 100f))
        {
            print("We hit something at " + hit.transform.position);
        } else
        {
            print("Nothing under us");
        }
	}
}
